// Override Settings
var bcSfFilterSettings = {
  general: {
    limit: bcSfFilterConfig.custom.products_per_page,
    /* Optional */
    loadProductFirst: false,
  }
};

// Declare Templates
var bcSfFilterTemplate = {
  'soldOutClass': 'sold-out',
  'saleClass': 'on-sale',

  // Grid Template
  'productGridItemHtml': '<div class="Grid__Cell {{sizeClasses}}">' +
                            '<div class="ProductItem">' +
                              '<div class="ProductItem__Wrapper {{soldOutClass}} {{saleClass}}">' +
                                '<a href="{{linkUrl}}" class="ProductItem__ImageWrapper {{altImageClass}}">' +
                                  '<div class="AspectRatio {{aspectRatioClass}}" style="{{aspectRatioStyle}}">' +
                                    '{{altImg}}' +
                                    '{{mainImg}}' + 
                                    '<span class="Image__Loader"></span>' +
                                  '</div>' +
                                '</a>' +
                                '{{productLabels}}' +
                                '<div class="ProductItem__Info ProductItem__Info--center">' +
                                  '<h2 class="ProductItem__Title Heading">' +
                                    '<a href="{{linkUrl}}">{{itemTitle}}</a>' +
                                  '</h2>' +
                                  '{{itemPrice}}' +
  								  '{{itemColorSwatches}}' +
                                '</div>' +
                              '</div>' +
                            '</div>' +
                          '</div>',

  // Pagination Template
  'previousActiveHtml': '<li><a href="{{itemUrl}}">&larr;</a></li>',
  'previousDisabledHtml': '<li class="disabled"><span>&larr;</span></li>',
  'nextActiveHtml': '<li><a href="{{itemUrl}}">&rarr;</a></li>',
  'nextDisabledHtml': '<li class="disabled"><span>&rarr;</span></li>',
  'pageItemHtml': '<li><a href="{{itemUrl}}">{{itemTitle}}</a></li>',
  'pageItemSelectedHtml': '<li><span class="active">{{itemTitle}}</span></li>',
  'pageItemRemainHtml': '<li><span>{{itemTitle}}</span></li>',
  'paginateHtml': '<ul class="pagination-custom">{{previous}}{{pageItems}}{{next}}</ul>',

  // Sorting Template
  'sortingHtml': '<label class="label--hidden">' + bcSfFilterConfig.label.sorting + '</label><select class="collection-sort__input">{{sortingItems}}</select>',
};

/************************** BUILD PRODUCT LIST **************************/

// Build Product Grid Item
BCSfFilter.prototype.buildProductGridItem = function(data, index) {
  /*** Prepare data ***/
  var images = data.images_info;
  var soldOut = !data.available; // Check a product is out of stock
  var onSale = data.compare_at_price_min > data.price_min; // Check a product is on sale
  var priceVaries = data.price_min != data.price_max; // Check a product has many prices
  // Get First Variant (selected_or_first_available_variant)
  var firstVariant = data['variants'][0];
  if (getParam('variant') !== null && getParam('variant') != '') {
      var paramVariant = data.variants.filter(function(e) { return e.id == getParam('variant'); });
      if (typeof paramVariant[0] !== 'undefined') firstVariant = paramVariant[0];
  } else {
      for (var i = 0; i < data['variants'].length; i++) {
          if (data['variants'][i].available) {
              firstVariant = data['variants'][i];
              break;
          }
      }
  }
  /*** End Prepare data ***/

  // Get Template
  var itemHtml = bcSfFilterTemplate.productGridItemHtml;

  // Add Thumbnail
  var itemThumbUrl = images.length > 0 ? this.optimizeImage(images[0]['src']) : bcSfFilterConfig.general.no_image_url;
  itemHtml = itemHtml.replace(/{{itemThumbUrl}}/g, itemThumbUrl);

  // Add Price
  var itemPriceHtml = '';
  if (data.title != '')  {
    itemPriceHtml += '<div class="ProductItem__PriceList Heading">';
    if (data.price_varies) {
      itemPriceHtml += '<span class="ProductItem__Price Price Text--subdued" data-money-convertible>' + this.formatMoney(data.price_min) + ' - ' + this.formatMoney(data.price_max) + '</span>';
    } else if (onSale) {
      itemPriceHtml += '<span class="ProductItem__Price Price Price--highlight Text--subdued" data-money-convertible>' + this.formatMoney(data.price_min) + '</span>';
      itemPriceHtml += '<span class="ProductItem__Price Price Price--compareAt Text--subdued" data-money-convertible>' + this.formatMoney(data.compare_at_price_min) + '</span> ';
    } else {
      itemPriceHtml += '<span class="ProductItem__Price Price Text--subdued" data-money-convertible>' + this.formatMoney(data.price_min) + '</span>';
    }
    itemPriceHtml += '</div>';
  }
  itemHtml = itemHtml.replace(/{{itemPrice}}/g, itemPriceHtml);

  // Add soldOut class
  var soldOutClass = soldOut ? bcSfFilterTemplate.soldOutClass : '';
  itemHtml = itemHtml.replace(/{{soldOutClass}}/g, soldOutClass);

  // Add onSale class
  var saleClass = onSale ? bcSfFilterTemplate.saleClass : '';
  itemHtml = itemHtml.replace(/{{saleClass}}/g, saleClass);

  // Add soldOut Label
  var itemSoldOutLabelHtml = soldOut ? bcSfFilterTemplate.soldOutLabelHtml : '';
  itemHtml = itemHtml.replace(/{{itemSoldOutLabel}}/g, itemSoldOutLabelHtml);

  // Add onSale Label
  var itemSaleLabelHtml = onSale ? bcSfFilterTemplate.saleLabelHtml : '';
  itemHtml = itemHtml.replace(/{{itemSaleLabel}}/g, itemSaleLabelHtml);

  // grid size
  itemHtml = itemHtml.replace(/{{sizeClasses}}/g, buildGridWidthClass());
  // alt image class
  itemHtml = itemHtml.replace(/{{altImageClass}}/g, buildAltImageClass());
  // add aspect ratio class and style
  itemHtml = itemHtml.replace(/{{aspectRatioClass}}/g, buildAspectRatioClass());
  itemHtml = itemHtml.replace(/{{aspectRatioStyle}}/g, buildAspectRatioStyle(data));

  // build images
  var fetchedImage = data.images[1] ? data.images[1] : data.images[0];
  itemHtml = itemHtml.replace(/{{altImg}}/g, buildImages(fetchedImage, true));
  itemHtml = itemHtml.replace(/{{mainImg}}/g, buildImages(data.featured_image, false));

  // build labels
  itemHtml = itemHtml.replace(/{{productLabels}}/g, buildLabels(data));
  
  //build color swatches
  itemHtml = itemHtml.replace(/{{itemColorSwatches}}/g, buildColorSwatches(data, index));
  

  // Add Vendor
  var itemVendorHtml = bcSfFilterConfig.custom.vendor_enable ? bcSfFilterTemplate.vendorHtml.replace(/{{itemVendorLabel}}/g, data.vendor) : '';
  itemHtml = itemHtml.replace(/{{itemVendor}}/g, itemVendorHtml);

  // Add main attribute (Always put at the end of this function)
  itemHtml = itemHtml.replace(/{{itemId}}/g, data.id);
  itemHtml = itemHtml.replace(/{{itemHandle}}/g, data.handle);
  itemHtml = itemHtml.replace(/{{itemTitle}}/g, data.title);
  itemHtml = itemHtml.replace(/{{itemUrl}}/g, this.buildProductItemUrl(data));
  itemHtml = itemHtml.replace(/{{linkUrl}}/g, buildUrl(data));

  return itemHtml;
};

// Build Product List Item
BCSfFilter.prototype.buildProductListItem = function(data) {
  // // Add Description
  // var itemDescription = jQ('<p>' + data.body_html + '</p>').text();
  // // Truncate by word
  // itemDescription = (itemDescription.split(" ")).length > 51 ? itemDescription.split(" ").splice(0, 51).join(" ") + '...' : itemDescription.split(" ").splice(0, 51).join(" ");
  // // Truncate by character
  // itemDescription = itemDescription.length > 350 ? itemDescription.substring(0, 350) + '...' : itemDescription.substring(0, 350);
  // itemHtml = itemHtml.replace(/{{itemDescription}}/g, itemDescription);
};

/************************** END BUILD PRODUCT LIST **************************/

function buildUrl(data) {
  var path = window.location.pathname;
  var handle = data.handle;

  var split = path.split("/");
  return "/collections/" + split[2] + "/products/" + handle;
}

// Build Grid Class
function buildGridWidthClass() {
    var mobileItemsPerRow = bcSfFilterConfig.custom.mobile_items_per_row;
    var tabletItemsPerRow = bcSfFilterConfig.custom.tablet_items_per_row;
    var desktopItemsPerRow = bcSfFilterConfig.custom.desktop_items_per_row;
    var filterPosition = bcSfFilterConfig.custom.filter_position;

    return '1/' + mobileItemsPerRow + '--phone 1/' + tabletItemsPerRow + '--tablet-and-up 1/' + desktopItemsPerRow + '--' + filterPosition;
}

// create alt image class if it exists
function buildAltImageClass() {
    var useAltImages = bcSfFilterConfig.custom.use_alternate_images;
    
    return useAltImages ? 'ProductItem__ImageWrapper--withAlternateImage' : '';
}

// build ratio setup 
function buildAspectRatioClass() {
  var productImgSize = bcSfFilterConfig.custom.product_image_size;
  var useNaturalSize = bcSfFilterConfig.custom.use_natural_size;
  var styleExt = useNaturalSize ? 'withFallback' : productImgSize;

  return 'AspectRatio--' + styleExt;
}

function buildAspectRatioStyle(data) {
  var featuredImage = data.featured_image;
  var useNaturalSize = bcSfFilterConfig.custom.use_natural_size;
  var aspectRatio = featuredImage.width / featuredImage.height;
  var featuredImgWidth =  featuredImage.width ? 'max-width: ' + featuredImage.width + 'px; ' : '';
  var aspectRatioPadding = useNaturalSize ? 'padding-bottom: ' + 100.0 / aspectRatio + '%; ' : '';
  var aspectRatioStyle = featuredImage.width ? '--aspect-ratio: ' + aspectRatio + ';' : '';

  return featuredImgWidth + aspectRatioPadding + aspectRatioStyle;
}

// create product image
function buildImages(image, alt) {
  var altClass = alt ? 'ProductItem__Image--alternate ' : '';
  return '<img class="ProductItem__Image ' + altClass + 'Image--lazyLoad Image--fadeIn" data-src="' + image.src + '" ata-image-id="' + image.id +'" />';
}

//check if ecomm tag exists
function isEcommExclusive(tags) {
  var ecommTag = 'Ecomm Exclusive' || 'ecomm exlusive';
  if (tags.includes(ecommTag)) {
    return true;
  }
  return false;
}

// create sale tags and other labels
function buildLabels(data) {
  var available = data.available
  var onSale = data.compare_at_price_min > data.price_min;
  var ecommExclusive = isEcommExclusive(data.tags);
  var rightClass = ecommExclusive ? ' ProductItem__LabelList--right' : '';
  var labelAvailable = (!available || onSale || ecommExclusive);

  if (labelAvailable) {
    var labelHtml = '<div class="ProductItem__LabelList' + rightClass + '">';
    if (!available) {
      labelHtml += '<span class="ProductItem__Label Heading Text--subdued">' +
                        bcSfFilterConfig.label.sold_out +
                     '</span>';
    } else if (ecommExclusive) {
      labelHtml += '<span class="ProductItem__Label ProductItem__Label-Eccom-Exclusive Heading Text--subdued"><span class="mobile-break">Online </span>Exclusive</span>';
    } else if (onSale) {
      labelHtml += '<span class="ProductItem__Label Heading Text--subdued">' +
                      bcSfFilterConfig.label.sale +
                   '</span>';
    }
    labelHtml += '</div>';
    return labelHtml;
  }
  return '';
}

//create each color swatch
function buildColorSwatches(data, index) {
  var selectedValue = data.selected_or_first_available_variant.option_color;
  var colorInt = 1;
  for (var i = 0; i < data.options.length; i++) {
    if (data.options[i] === 'color') {
      colorInt = i;
    }
  }
  var colorSwatchHtml = '<ul class="ProductItem__Swatches HorizontalList HorizontalList--spacingTight">';
    var collectionName = jQ('.boost-pfs-collection-name').attr('data-section-name');
  console.log(collectionName);
    if(collectionName == 'accessories'){
  	colorSwatchHtml += '</ul>';
  } else {
  for (var c = 0; c < data.options_with_values[colorInt].values.length; c++ ) {
    var swatch = data.options_with_values[colorInt].values[c];
    var isChecked = selectedValue === swatch.title;
    var isActive = isChecked ? ' is-active' : ''; 
    var swatchSlug = swatch.title.replace(/[\/ ]/g, "-").toLowerCase();
    colorSwatchHtml += '<li class="HorizontalList__Item ' + swatchSlug + '">' +
                         //'<input id="option-' + data.id + '-' + swatchSlug + '" class="ColorSwatch__Radio" type="radio" name="option-' + data.id + '" value="' + swatch.title + '" ' + isChecked + 'data-image-position="' + swatch.image + '" data-index="' + index + '">' +
                         '<div class="ColorSwatch ColorSwatch--small' + isActive + '" data-tooltip="' + swatch.title + '" data-image-position="' + swatch.image + '" data-index="' + index + '">' +
                           '<span class="u-visually-hidden">' + swatch.title + '</span>' +
                         '</div>' +
                       '</li>';
  }
  colorSwatchHtml += '</ul>';
  }
  
  return colorSwatchHtml;
}

// Build Pagination
BCSfFilter.prototype.buildPagination = function(totalProduct) {
  if (this.getSettingValue('general.paginationType') == 'default') {
      // Get page info
      var currentPage = parseInt(this.queryParams.page);
      var totalPage = Math.ceil(totalProduct / this.queryParams.limit);

      // If it has only one page, clear Pagination
      if (totalPage == 1) {
          jQ(this.selector.bottomPagination).html('');
          return false;
      }

      if (this.getSettingValue('general.paginationType') == 'default') {
          var paginationHtml = bcSfFilterTemplate.paginateHtml;

          // Build Previous
          var previousHtml = (currentPage > 1) ? bcSfFilterTemplate.previousActiveHtml : bcSfFilterTemplate.previousDisabledHtml;
          previousHtml = previousHtml.replace(/{{itemUrl}}/g, this.buildToolbarLink('page', currentPage, currentPage - 1));
          paginationHtml = paginationHtml.replace(/{{previous}}/g, previousHtml);

          // Build Next
          var nextHtml = (currentPage < totalPage) ? bcSfFilterTemplate.nextActiveHtml :  bcSfFilterTemplate.nextDisabledHtml;
          nextHtml = nextHtml.replace(/{{itemUrl}}/g, this.buildToolbarLink('page', currentPage, currentPage + 1));
          paginationHtml = paginationHtml.replace(/{{next}}/g, nextHtml);

          // Create page items array
          var beforeCurrentPageArr = [];
          for (var iBefore = currentPage - 1; iBefore > currentPage - 3 && iBefore > 0; iBefore--) {
              beforeCurrentPageArr.unshift(iBefore);
          }
          if (currentPage - 4 > 0) {
              beforeCurrentPageArr.unshift('...');
          }
          if (currentPage - 4 >= 0) {
              beforeCurrentPageArr.unshift(1);
          }
          beforeCurrentPageArr.push(currentPage);

          var afterCurrentPageArr = [];
          for (var iAfter = currentPage + 1; iAfter < currentPage + 3 && iAfter <= totalPage; iAfter++) {
              afterCurrentPageArr.push(iAfter);
          }
          if (currentPage + 3 < totalPage) {
              afterCurrentPageArr.push('...');
          }
          if (currentPage + 3 <= totalPage) {
              afterCurrentPageArr.push(totalPage);
          }

          // Build page items
          var pageItemsHtml = '';
          var pageArr = beforeCurrentPageArr.concat(afterCurrentPageArr);
          for (var iPage = 0; iPage < pageArr.length; iPage++) {
              if (pageArr[iPage] == '...') {
                  pageItemsHtml += bcSfFilterTemplate.pageItemRemainHtml;
              } else {
                  pageItemsHtml += (pageArr[iPage] == currentPage) ? bcSfFilterTemplate.pageItemSelectedHtml : bcSfFilterTemplate.pageItemHtml;
              }
              pageItemsHtml = pageItemsHtml.replace(/{{itemTitle}}/g, pageArr[iPage]);
              pageItemsHtml = pageItemsHtml.replace(/{{itemUrl}}/g, this.buildToolbarLink('page', currentPage, pageArr[iPage]));
          }
          paginationHtml = paginationHtml.replace(/{{pageItems}}/g, pageItemsHtml);

          jQ(this.selector.bottomPagination).html(paginationHtml);
      }
  }
};

/************************** BUILD TOOLBAR **************************/

// Build Sorting
BCSfFilter.prototype.buildFilterSorting = function() {
  if (bcSfFilterTemplate.hasOwnProperty('sortingHtml')) {
      jQ(this.selector.topSorting).html('');

      var sortingArr = this.getSortingList();
      if (sortingArr) {
          // Build content 
          var sortingItemsHtml = '';
          for (var k in sortingArr) {
              sortingItemsHtml += '<option value="' + k +'">' + sortingArr[k] + '</option>';
          }
          var html = bcSfFilterTemplate.sortingHtml.replace(/{{sortingItems}}/g, sortingItemsHtml);
          jQ(this.selector.topSorting).html(html);

          // Set current value
          jQ(this.selector.topSorting + ' select').val(this.queryParams.sort);
      }
  }
};

// Build Display type (List / Grid / Collage)
// BCSfFilter.prototype.buildFilterDisplayType = function() {
//     var itemHtml = '<a href="' + this.buildToolbarLink('display', 'list', 'grid') + '" title="Grid view" class="change-view bc-sf-filter-display-grid" data-view="grid"><span class="icon-fallback-text"><i class="fa fa-th" aria-hidden="true"></i><span class="fallback-text">Grid view</span></span></a>';
//     itemHtml += '<a href="' + this.buildToolbarLink('display', 'grid', 'list') + '" title="List view" class="change-view bc-sf-filter-display-list" data-view="list"><span class="icon-fallback-text"><i class="fa fa-list" aria-hidden="true"></i><span class="fallback-text">List view</span></span></a>';
//     jQ(this.selector.topDisplayType).html(itemHtml);

//     // Active current display type
//     jQ(this.selector.topDisplayType).find('.bc-sf-filter-display-list').removeClass('active');
//     jQ(this.selector.topDisplayType).find('.bc-sf-filter-display-grid').removeClass('active');
//     if (this.queryParams.display == 'list') {
//         jQ(this.selector.topDisplayType).find('.bc-sf-filter-display-list').addClass('active');
//     } else if (this.queryParams.display == 'grid') {
//         jQ(this.selector.topDisplayType).find('.bc-sf-filter-display-grid').addClass('active');
//     }
// };

/************************** END BUILD TOOLBAR **************************/

// Build Default layout
function buildDefaultLink(a,b){var c=window.location.href.split("?")[0];return c+="?"+a+"="+b}BCSfFilter.prototype.buildDefaultElements=function(a){if(bcSfFilterConfig.general.hasOwnProperty("collection_count")&&jQ("#bc-sf-filter-bottom-pagination").length>0){var b=bcSfFilterConfig.general.collection_count,c=parseInt(this.queryParams.page),d=Math.ceil(b/this.queryParams.limit);if(1==d)return jQ(this.selector.pagination).html(""),!1;if("default"==this.getSettingValue("general.paginationType")){var e=bcSfFilterTemplate.paginateHtml,f="";f=c>1?bcSfFilterTemplate.hasOwnProperty("previousActiveHtml")?bcSfFilterTemplate.previousActiveHtml:bcSfFilterTemplate.previousHtml:bcSfFilterTemplate.hasOwnProperty("previousDisabledHtml")?bcSfFilterTemplate.previousDisabledHtml:"",f=f.replace(/{{itemUrl}}/g,buildDefaultLink("page",c-1)),e=e.replace(/{{previous}}/g,f);var g="";g=c<d?bcSfFilterTemplate.hasOwnProperty("nextActiveHtml")?bcSfFilterTemplate.nextActiveHtml:bcSfFilterTemplate.nextHtml:bcSfFilterTemplate.hasOwnProperty("nextDisabledHtml")?bcSfFilterTemplate.nextDisabledHtml:"",g=g.replace(/{{itemUrl}}/g,buildDefaultLink("page",c+1)),e=e.replace(/{{next}}/g,g);for(var h=[],i=c-1;i>c-3&&i>0;i--)h.unshift(i);c-4>0&&h.unshift("..."),c-4>=0&&h.unshift(1),h.push(c);for(var j=[],k=c+1;k<c+3&&k<=d;k++)j.push(k);c+3<d&&j.push("..."),c+3<=d&&j.push(d);for(var l="",m=h.concat(j),n=0;n<m.length;n++)"..."==m[n]?l+=bcSfFilterTemplate.pageItemRemainHtml:l+=m[n]==c?bcSfFilterTemplate.pageItemSelectedHtml:bcSfFilterTemplate.pageItemHtml,l=l.replace(/{{itemTitle}}/g,m[n]),l=l.replace(/{{itemUrl}}/g,buildDefaultLink("page",m[n]));e=e.replace(/{{pageItems}}/g,l),jQ(this.selector.pagination).html(e)}}if(bcSfFilterTemplate.hasOwnProperty("sortingHtml")&&jQ(this.selector.topSorting).length>0){jQ(this.selector.topSorting).html("");var o=this.getSortingList();if(o){var p="";for(var q in o)p+='<option value="'+q+'">'+o[q]+"</option>";var r=bcSfFilterTemplate.sortingHtml.replace(/{{sortingItems}}/g,p);jQ(this.selector.topSorting).html(r);var s=void 0!==this.queryParams.sort_by?this.queryParams.sort_by:this.defaultSorting;jQ(this.selector.topSorting+" select").val(s),jQ(this.selector.topSorting+" select").change(function(a){window.location.href=buildDefaultLink("sort_by",jQ(this).val())})}}};

// Customize data to suit the data of Shopify API
BCSfFilter.prototype.prepareProductData=function(data){for(var k=0;k<data.length;k++){data[k]['images']=data[k]['images_info'];if(data[k]['images'].length>0){data[k]['featured_image']=data[k]['images'][0]}else{data[k]['featured_image']={src:bcSfFilterConfig.general.no_image_url,width:'',height:'',aspect_ratio:0}}data[k]['url']='/products/'+data[k].handle;var optionsArr=[];for(var i=0;i<data[k]['options_with_values'].length;i++){optionsArr.push(data[k]['options_with_values'][i]['name'])}data[k]['options']=optionsArr;data[k]['price_min']*=100,data[k]['price_max']*=100,data[k]['compare_at_price_min']*=100,data[k]['compare_at_price_max']*=100;data[k]['price']=data[k]['price_min'];data[k]['compare_at_price']=data[k]['compare_at_price_min'];data[k]['price_varies']=data[k]['price_min']!=data[k]['price_max'];var firstVariant=data[k]['variants'][0];if(getParam('variant')!==null&&getParam('variant')!=''){var paramVariant=data.variants.filter(function(e){return e.id==getParam('variant')});if(typeof paramVariant[0]!=='undefined')firstVariant=paramVariant[0]}else{for(var i=0;i<data[k]['variants'].length;i++){if(data[k]['variants'][i].available){firstVariant=data[k]['variants'][i];break}}}data[k]['selected_or_first_available_variant']=firstVariant;for(var i=0;i<data[k]['variants'].length;i++){var variantOptionArr=[];var count=1;var variant=data[k]['variants'][i];var variantOptions=variant['merged_options'];if(Array.isArray(variantOptions)){for(var j=0;j<variantOptions.length;j++){var temp=variantOptions[j].split(':');data[k]['variants'][i]['option'+(parseInt(j)+1)]=temp[1];data[k]['variants'][i]['option_'+temp[0]]=temp[1];variantOptionArr.push(temp[1])}data[k]['variants'][i]['options']=variantOptionArr}data[k]['variants'][i]['compare_at_price']=parseFloat(data[k]['variants'][i]['compare_at_price'])*100;data[k]['variants'][i]['price']=parseFloat(data[k]['variants'][i]['price'])*100}data[k]['description']=data[k]['content']=data[k]['body_html']}return data};

// Add additional feature for product list, used commonly in customizing product list
BCSfFilter.prototype.buildExtrasProductList = function(data, eventType) {
  jQ('.ColorSwatch').click(function() {
    jQ(this).parents('.ProductItem__Swatches').find('.ColorSwatch').removeClass('is-active');
    jQ(this).addClass('is-active');
    
    var imgNum = jQ(this).data('image-position') || 1;
    var dataNum = jQ(this).data('index') - 1;
    var prodUrl = BCSfFilter.prototype.buildProductItemUrl(data[dataNum]);
    if (imgNum) {
      var imgSrc = data[dataNum].images_info[0].src;
      var altImgSrc = data[dataNum].images_info[1].src;
      for (var pos = 0; pos < data[dataNum].images_info.length; pos++) {
        if (data[dataNum].images_info[pos].position === imgNum) {
          imgSrc = data[dataNum].images_info[pos].src;
          altImgSrc = data[dataNum].images_info[pos + 1].src;
        }
      }
      var productItem = jQ(this).parents('.ProductItem')
      var imgWrap = jQ(productItem).find('.AspectRatio');
      jQ(imgWrap).find('.ProductItem__Image--alternate').attr('src', altImgSrc);
      jQ(imgWrap).find('.ProductItem__Image').not('.ProductItem__Image--alternate').attr('src', imgSrc);
    }
  });
};




// Build additional elements
BCSfFilter.prototype.buildAdditionalElements = function(data, eventType) {};

// Fix image url issue of swatch option
function getFilePath(fileName, ext, version) {
    var self = bcsffilter;
    var ext = typeof ext !== 'undefined' ? ext : 'png';
    var version = typeof version !== 'undefined' ? version : '1';
    var prIndex = self.fileUrl.lastIndexOf('?');
    if (prIndex > 0) {
        var filePath = self.fileUrl.substring(0, prIndex);
    } else {
        var filePath = self.fileUrl;
    }
    filePath += fileName + '.' + ext + '?v=' + version;
    return filePath;
}

BCSfFilter.prototype.getFilterData=function(eventType,errorCount){function BCSend(eventType,errorCount){var self=bcsffilter;var errorCount=typeof errorCount!=="undefined"?errorCount:0;self.showLoading();if(typeof self.buildPlaceholderProductList=="function"){self.buildPlaceholderProductList(eventType)}self.beforeGetFilterData(eventType);self.prepareRequestParams(eventType);self.queryParams["callback"]="BCSfFilterCallback";self.queryParams["event_type"]=eventType;var url=self.isSearchPage()?self.getApiUrl("search"):self.getApiUrl("filter");var script=document.createElement("script");script.type="text/javascript";var timestamp=(new Date).getTime();script.src=url+"?t="+timestamp+"&"+jQ.param(self.queryParams);script.id="bc-sf-filter-script";script.async=true;var resendAPITimer,resendAPIDuration;resendAPIDuration=2e3;script.addEventListener("error",function(e){if(typeof document.getElementById(script.id).remove=="function"){document.getElementById(script.id).remove()}else{document.getElementById(script.id).outerHTML=""}if(errorCount<3){errorCount++;if(resendAPITimer){clearTimeout(resendAPITimer)}resendAPITimer=setTimeout(self.getFilterData("resend",errorCount),resendAPIDuration)}else{self.buildDefaultElements(eventType)}});document.getElementsByTagName("head")[0].appendChild(script);script.onload=function(){if(typeof document.getElementById(script.id).remove=="function"){document.getElementById(script.id).remove()}else{document.getElementById(script.id).outerHTML=""}}}this.requestFilter(BCSend,eventType,errorCount)};BCSfFilter.prototype.requestFilter=function(sendFunc,eventType,errorCount){sendFunc(eventType,errorCount)};


/* start-boost-2.4.8 */
BCSfFilter.prototype.buildFilterOptionItem=function(html,iLabel,iValue,fOType,fOId,fOLabel,fODisplayType,fOSelectType,fOItemValue,fOData){var keepValuesStatic=fOData.hasOwnProperty("keepValuesStatic")?fOData.keepValuesStatic:false;if(fOType=="review_ratings"&&this.getSettingValue("general.ratingSelectionStyle")=="text"){var title=this.getReviewRatingsLabel(fOItemValue.from)}else{var title=this.customizeFilterOptionLabel(iLabel,fOData.prefix,fOType)}if(keepValuesStatic===true)var productNumber=null;else var productNumber=fOItemValue.hasOwnProperty("doc_count")?fOItemValue.doc_count:0;html=html.replace(/{{itemLabel}}/g,this.buildFilterOptionLabel(iLabel,productNumber,fOData));html=html.replace(/{{itemLink}}/g,this.buildFilterOptionLink(fOId,iValue,fOType,fODisplayType,fOSelectType,keepValuesStatic));html=html.replace(/{{itemValue}}/g,encodeURIParamValue(iValue));html=html.replace(/{{itemTitle}}/g,title);html=html.replace(/{{itemFunc}}/g,"onInteractWithFilterOptionValue(event, this, '"+fOType+"', '"+fODisplayType+"', '"+fOSelectType+"', '"+keepValuesStatic+"')");html=this.checkFilterOptionSelected(fOId,iValue,fOType,fODisplayType)?html.replace(/{{itemSelected}}/g,"selected"):html.replace(/{{itemSelected}}/g,"");var htmlElement=jQ(html);htmlElement.children().attr({"data-id":fOId,"data-value":encodeURIParamValue(iValue),"data-parent-label":fOLabel,"data-title":title,"data-count":productNumber});if(fOType!="collection"){htmlElement.children().attr("rel","nofollow")}if(fOType=="collection")htmlElement.children().attr("data-collection-scope",fOItemValue.key);return jQ(htmlElement)[0].outerHTML};
/* end-boost-2.4.8 */

/* Begin patch boost-010 run 2 */
BCSfFilter.prototype.initFilter=function(){return this.isBadUrl()?void(window.location.href=window.location.pathname):(this.updateApiParams(!1),void this.getFilterData("init"))},BCSfFilter.prototype.isBadUrl=function(){try{var t=decodeURIComponent(window.location.search).split("&"),e=!1;if(t.length>0)for(var i=0;i<t.length;i++){var n=t[i],a=(n.match(/</g)||[]).length,r=(n.match(/>/g)||[]).length,o=(n.match(/alert\(/g)||[]).length,h=(n.match(/execCommand/g)||[]).length;if(a>0&&r>0||a>1||r>1||o||h){e=!0;break}}return e}catch(l){return!0}};
/* End patch boost-010 run 2 */
